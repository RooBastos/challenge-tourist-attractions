export class AttractionsList {
    constructor() {
        this.list = [];
        this.imageSelected = "";

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".attractions-form");
        this.imageInput = document.querySelector(".attractions-form-file-input");
        this.titleInput = document.querySelector(".attractions-form-title-input");
        this.descriptionInput = document.querySelector(".attractions-form-description-input");
        this.items = document.querySelector(".attractions-items");
        // this.imageRender = document.querySelector(".displayImage");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));

        this.imageInput.addEventListener("change", (e) => {
            this.uploadImage(e);
        });
    }

    convertBase64(file) {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    }

    uploadImage = async (event) => {
        const file = event.target.files[0];
        const base64 = await this.convertBase64(file);

        this.imageSelected = base64;
    };

    addItemToList(event) {
        event.preventDefault();

        const itemTitle = event.target["title"].value;
        const itemDescription = event.target["description"].value;

        if (itemTitle !== "" && itemDescription !== "") {
            const item = {
                image: this.imageSelected,
                title: itemTitle,
                description: itemDescription,
            };

            this.list.push(item);

            this.renderListItens();
            this.resetInputs();
        }
    }

    renderListItens() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                <li class="attraction-item">
                    <img src="${item.image}" class="displayImage"/>
                    <div class="attractive-item-wrapper">
                    <span> <h3 class="attraction-item-title">${item.title}<h3></span>
                    <span> <p class="attraction-item-description">${item.description}</p></span>
                    </div
                </li>
            `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
    }
}
